# JSONP接口安全

## JSONP接口潜在危害

1. xss攻击
2. CSRF攻击

### xss攻击

输出 JSON 时，没有严格定义好 Content-Type（Content-Type: application/json）直接导致了一个典型的 XSS 漏洞

### CSRF攻击

当被攻击者在登陆 360 网站的情况下访问了该网页时，那么用户的隐私数据（如用户名，邮箱等）可能被攻击者劫持

## JSONP接口安全措施

1. refer强校验
2. token校验
3. content-type校验 , 不能是 text/html
4. callback 校验
5. 过滤JSONP接口返回, 使用框架生成的

## 资料

[JSONP使用以及注意的安全问题](https://blog.csdn.net/netyeaxi/article/details/79855814)

[JSONP攻防技术](https://blog.knownsec.com/2015/03/jsonp_security_technic/)