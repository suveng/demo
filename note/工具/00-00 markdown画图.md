# markdown mermaid 画图

## 流程图 flow chart

时序图代码如下, 写到typroa下面即可, 查看源码是这种格式即可

~~~shell
```flow
st=>start: 开始
op=>operation: 普通操作
cond=>condition: 结果是否成功?
op2=>operation: 操作2
op3=>operation: 操作3
sub1=>subroutine: 子程序1:>url
i1=>inputoutput: inputoutput1
e=>end: 结束
st->op->cond->
cond(yes)->op2->e
cond(no)->op3->sub1->i1->e
```
~~~



```flow
st=>start: 开始
op=>operation: 普通操作
cond=>condition: 结果是否成功?
op2=>operation: 操作2
op3=>operation: 操作3
sub1=>subroutine: 子程序1:>url
i1=>inputoutput: inputoutput1
e=>end: 结束
st->op->cond->
cond(yes)->op2->e
cond(no)->op3->sub1->i1->e
```

#### 定义元素的语法

```bash
tag=>type: content:>url
```

tag就是元素名字，

type是这个元素的类型，有6中类型，分别为

1. start # 开始
2. end # 结束
3. operation # 操作
4. subroutine # 子程序
5. condition # 条件
6. inputoutput # 输入或产出
7. content就是在框框中要写的内容，注意type后的冒号与文本之间一定要有个空格。
8. url是一个连接，与框框中的文本相绑定

#### 连接元素的语法

```php
c2(yes)->io->e 
c2(no)->op2->e
```
## 时序图 sequence diagram

时序图代码如下, 写到typroa下面即可, 查看源码是这种格式即可

```shell
​```mermaid
sequenceDiagram
title: 提现流程图
client->> + web: 1. 跳转提现页面
web->> + server: 2. 发起提现
server --> - web : 3. 返回提现结果
note over web : 提现结果处理
alt 提现失败
web ->> web : 4. 提现失败处理
note over web : 详细查看 提现失败处理时序图
else 提现成功
web ->> web : 5.通知成功
note over web : 详细查看 提现成功处理时序图
end
web -->>- client : 6.主动退出
​```
```

```mermaid
sequenceDiagram
title: 提现流程图
client->> + web: 1. 跳转提现页面
web->> + server: 2. 发起提现
server --> - web : 3. 返回提现结果
note over web : 提现结果处理
alt 提现失败
web ->> web : 4. 提现失败处理
note over web : 详细查看 提现失败处理时序图
else 提现成功
web ->> web : 5.通知成功
note over web : 详细查看 提现成功处理时序图
end
web -->>- client : 6.主动退出
```



### 标题

- title
  图的标题

```shell
sequenceDiagram
title: 图的标题
participant participant
```



```mermaid
sequenceDiagram
title: 图的标题
participant participant
```



### 参与者

- participant
  可以通过这个来启用别名

```shell
sequenceDiagram
title: 图的标题
participant p1 as participant1
participant p2 as participant2
p1->p2: 链接
```

```mermaid
sequenceDiagram
title: 图的标题
participant p1 as participant1
participant p2 as participant2
p1->p2: 链接
```



### 备注方向控制

- left of
  生命线的左边

- right of
  生命线的右边

- over 
  生命线中间

```shell
sequenceDiagram
title: 图的标题
participant p1 as participant1
participant p2 as participant2
note left of  p1: left
note right of p2: right
note over p2 : over
p1->p2: 链接
```

```mermaid
sequenceDiagram
title: 图的标题
participant p1 as participant1
participant p2 as participant2
note left of  p1: left
note right of p2: right
note over p2 : over
p1->p2: 链接


```



### 循环

```shell
sequenceDiagram
title: 图的标题
participant p1 as participant1
participant p2 as participant2
note over p1: 准备循环
loop 条件
note over p2: notev2
note over p1: notev2
end
```

```mermaid
sequenceDiagram
title: 图的标题
participant p1 as participant1
participant p2 as participant2
note over p1: 准备循环
loop 条件
note over p2: notev2
note over p1: notev2
end
```



### 箭头

1. `->` 表示实线
2. `-->` 表示虚线
3. `->>` 表示实线箭头
4. `-->>` 表示虚线箭头

```shell
sequenceDiagram
title: 图的标题
participant p1 as participant1
participant p2 as participant2
p1 -> p2: 实线
p1-->p2: 虚线
p1 ->> p2: 实线箭头
p1-->>p2: 虚线箭头
```

```mermaid
sequenceDiagram
title: 图的标题
participant p1 as participant1
participant p2 as participant2
p1 -> p2: 实线
p1-->p2: 虚线
p1 ->> p2: 实线箭头
p1-->>p2: 虚线箭头
```



### 激活块

```shell
+ 开始激活块
- 结束激活块
```

```shell
sequenceDiagram
title: 图的标题
participant p1 as participant1
participant p2 as participant2
p1 -> +p2: 实线
p2-->> -p1: 返回
```

```mermaid
sequenceDiagram
title: 图的标题
participant p1 as participant1
participant p2 as participant2
p1 -> +p2: 实线
p2-->> -p1: 返回
```



## 类图 class diagram

用的比较少, 毕竟IDEA有直接生成的

```shell
classDiagram
    Animal <|-- Duck
    Animal <|-- Fish
    Animal <|-- Zebra
    Animal : +int age
    Animal : +String gender
    Animal: +isMammal()
    Animal: +mate()
    class Duck{
      +String beakColor
      +swim()
      +quack()
    }
    class Fish{
      -int sizeInFeet
      -canEat()
    }
    class Zebra{
      +bool is_wild
      +run()
    }
```



```mermaid
classDiagram
    Animal <|-- Duck
    Animal <|-- Fish
    Animal <|-- Zebra
    Animal : +int age
    Animal : +String gender
    Animal: +isMammal()
    Animal: +mate()
    class Duck{
      +String beakColor
      +swim()
      +quack()
    }
    class Fish{
      -int sizeInFeet
      -canEat()
    }
    class Zebra{
      +bool is_wild
      +run()
    }
            
```



## 状态图 state diagram

`[*]` 标识开始/结束

```shell
stateDiagram
    [*] --> Still
    Still --> [*]
    Still --> Moving
    Moving --> Still
    Moving --> Crash
    Crash --> [*]
```

```mermaid
stateDiagram
    [*] --> Still
    Still --> [*]
    Still --> Moving
    Moving --> Still
    Moving --> Crash
    Crash --> [*]
```




## 甘特图 gantt diagram

```shell
gantt
    title A Gantt Diagram
    dateFormat  YYYY-MM-DD
    section Section
    A task           :a1, 2020-01-01, 30d
    Another task     :after a1  , 20d
    section Another
    Task in sec      :2020-01-12  , 12d
    another task      : 24d
            
```

```mermaid
gantt
    title A Gantt Diagram
    dateFormat  YYYY-MM-DD
    section Section
    A task           :a1, 2020-01-01, 30d
    Another task     :after a1  , 20d
    section Another
    Task in sec      :2020-01-12  , 12d
    another task      : 24d
            
```

## 饼图 pie diagram

```shell
pie title Pets adopted by volunteers
    "Dogs" : 386
    "Cats" : 85
    "Rats" : 15
```



```mermaid
pie title Pets adopted by volunteers
    "Dogs" : 386
    "Cats" : 85
    "Rats" : 15
            
```



## ER图

```shell
erDiagram
          CUSTOMER }|..|{ DELIVERY-ADDRESS : has
          CUSTOMER ||--o{ ORDER : places
          CUSTOMER ||--o{ INVOICE : "liable for"
          DELIVERY-ADDRESS ||--o{ ORDER : receives
          INVOICE ||--|{ ORDER : covers
          ORDER ||--|{ ORDER-ITEM : includes
          PRODUCT-CATEGORY ||--|{ PRODUCT : contains
          PRODUCT ||--o{ ORDER-ITEM : "ordered in"
            
```

```mermaid
erDiagram
          CUSTOMER }|..|{ DELIVERY-ADDRESS : has
          CUSTOMER ||--o{ ORDER : places
          CUSTOMER ||--o{ INVOICE : "liable for"
          DELIVERY-ADDRESS ||--o{ ORDER : receives
          INVOICE ||--|{ ORDER : covers
          ORDER ||--|{ ORDER-ITEM : includes
          PRODUCT-CATEGORY ||--|{ PRODUCT : contains
          PRODUCT ||--o{ ORDER-ITEM : "ordered in"
            
```



## 资料

[基于mermaid的时序图,流程图, 甘特图](https://www.iminho.me/wiki/docs/mindoc/mermaid.md)

[mermaid时序图--知乎](https://zhuanlan.zhihu.com/p/70261692)

[mermaid在线图](https://mermaid-js.github.io/mermaid-live-editor/#/edit/eyJjb2RlIjoiZ3JhcGggVERcbkFbQ2hyaXN0bWFzXSAtLT58R2V0IG1vbmV5fCBCKEdvIHNob3BwaW5nKVxuQiAtLT4gQ3tMZXQgbWUgdGhpbmt9XG5DIC0tPnxPbmV8IERbTGFwdG9wXVxuQyAtLT58VHdvfCBFW2lQaG9uZV1cbkMgLS0-fFRocmVlfCBGW2ZhOmZhLWNhciBDYXJdXG4iLCJtZXJtYWlkIjp7InRoZW1lIjoiZGVmYXVsdCJ9fQ)

