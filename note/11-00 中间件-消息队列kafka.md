# 12-1 中间件-kafka

## 什么是kafka? 



### 架构是什么? 

### 角色

- 生产者
- kafka集群消息队列
  - zookeeper
  - broker
    - leader
    - follower
  - topic
  - partition
    - replication
    - offset
- 消费者

![img](https://gitee.com/suveng/upic/raw/master/uPic/f6930f44b67371737452043e4ab69eeb.jpg)

### 特性是什么? 

- 发布/订阅
- 流处理
- 分布式存储

### 应用场景是什么? 

- 消息队列
- 事件采集
- 监控
- 日志聚合
- 流处理
- 采集日志
- 提交日志

### 优缺点是什么? 



## 为什么会出现kafka? 



## 怎么使用kafka



### 安装

#### macos

**当前版本2.6**

<u>安装命令</u>

```bash
brew install kafka
```

> 安装完成后, 弹出提示
>
> To have launchd start kafka now and restart at login:
>   brew services start kafka
> Or, if you don't want/need a background service you can just run:
>   zookeeper-server-start /usr/local/etc/kafka/zookeeper.properties & kafka-server-start /usr/local/etc/kafka/server.properties



<u>安装位置</u>

`/usr/local/Cellar/kafka`

`/usr/local/Cellar/zookeeper`



<u>启动命令</u>

  `zookeeper-server-start /usr/local/etc/kafka/zookeeper.properties & kafka-server-start /usr/local/etc/kafka/server.properties`



仔细查看了文件指向, 源文件还是存放在`/usr/local/Cellar` 中



### 脚本基本使用kafka

#### 增删查改topic

查

```shell
# 查可用topic
bin/kafka-topics --bootstrap-server localhost:9092 --list

# 查看topic详情
bin/kafka-topics --bootstrap-server localhost:9092  --describe --topic xxx
```



增

```shell
bin/kafka-topics --bootstrap-server localhost:9092 --create --topic xxx \
--partitions 2 --replication-factor 3 
```



删

```shell
bin/kafka-topics --bootstrap-server localhost:9092 --delete --topic xxx
```



改

```shell
bin/kafka-topics --bootstrap-server localhost:9092  --alter --topic xxx \
--partitions 40
```



#### 发送消息

进入bin目录

```bash
# 查看帮助
kafka-console-producer --help
# 当前版本2.6
# required 参数
# --bootstrap-server kafka集群地址
# --topic topic名称
./kafka-console-producer --bootstrap-server localhost:9092 --topic xxx

# 继续输入消息, 按回车发送消息
```



#### 消费消息

```shell
# 查看帮助
bin/kafka-console-consumer --help

# 消费
bin/kafka-console-consumer --bootstrap-server localhost:9092 --topic xxx
```

### 事务性消息

1. Producer配置transactional.id
2. initTransactionas() 初始化
3. beiginTransaction() 事务开始
4. commitTransaction() 事务提交
5. abortTransaction() 事务回滚

### 优雅关机策略

1. 它将所有日志同步到磁盘，以避免在重新启动时需要进行任何日志恢复活动（即验证日志尾部的所有消息的校验和）。由于日志恢复需要时间，所以从侧面加速了重新启动操作。
2. 它将在关闭之前将以该服务器为 leader 的任何分区迁移到其他副本。这将使 leader 角色传递更快，并将每个分区不可用的时间缩短到几毫秒。

只要服务器的停止不是通过直接杀死，同步日志就会自动发生，但控制 leader 迁移需要使用特殊的设置：

```shell
controlled.shutdown.enable=true
```

请注意，只有当 broker 托管的分区具有副本（即，复制因子大于1 \*且至少其中一个副本处于活动状态）时，对关闭的控制才会成功。 这通常是你想要的，因为关闭最后一个副本会使 topic 分区不可用。

### balancing leadership

每当一个 borker 停止或崩溃时，该 borker 上的分区的leader 会转移到其他副本。这意味着，在 broker 重新启动时，默认情况下，它将只是所有分区的跟随者，这意味着它不会用于客户端的读取和写入

可以脚本操作

```shell
bin/kafka-preferred-replica-election.sh --zookeeper zk_host:port/chroot
```

可以配置

```shell
auto.leader.rebalance.enable=true
```

### 机架感知均衡分区副本

机架: rack

您可以通过向 broker 配置添加属性来指定 broker 属于的特定机架：

```shell
broker.rack=my-rack-id
```

当 topic 创建，修改或副本重新分配时， 机架约束将得到保证，确保副本跨越尽可能多的机架（一个分区将跨越 min(#racks，replication-factor) 个不同的机架）。



用于向 broker 分配副本的算法可确保每个 broker 的 leader 数量将保持不变，而不管 broker 在机架之间如何分布。这确保了均衡的吞吐量。



但是，如果 broker 在机架间分布不均 ，副本的分配将不均匀。具有较少 broker 的机架将获得更多复制副本，这意味着他们将使用更多存储并将更多资源投入复制。因此，每个机架配置相同数量的 broker 是明智的。

### 集群间同步数据

我们指的是通过“镜像”复制Kafka集群之间的数据的过程，以避免与在单个集群中的节点之间发生的复制混淆。Kafka附带了一个在Kafka集群之间镜像数据的工具 mirror-maker。该工具从源集群中消费数据并产生数据到目标集群。 这种镜像的常见用例是在另一个数据中心提供副本。



```shell
bin/kafka-mirror-maker.sh
      --consumer.config consumer.properties
      --producer.config producer.properties --whitelist my-topic
```

### 检查 consumer 位置

有时观察到消费者的位置是有用的。我们有一个工具，可以显示 consumer 群体中所有 consumer 的位置，以及他们所在日志的结尾。



```shell
 # 基于kafka
 bin/kafka-consumer-groups.sh --bootstrap-server localhost:9092 --describe --group my-group
 
 # 基于zookeeper
 bin/kafka-consumer-groups.sh --zookeeper localhost:2181 --describe --group my-group
```



### 管理 consumer group

```shell
bin/kafka-consumer-groups.sh --bootstrap-server localhost:9092 --list
```



### 扩容

1. 只需为其分配唯一的 broker ID
2. 在新服务器上启动Kafka即可

但是，这些新的服务器不会自动分配到任何数据分区，除非将分区移动到这些分区，否则直到创建新 topic 时才会提供服务。所以通常当您将机器添加到群集中时，您会希望将一些现有数据迁移到这些机器上。



分区重新分配工具可以以3种互斥方式运行：

1. --generate: 在此模式下，给定一个 topic 列表和一个 broker 列表，该工具会生成一个候选重新分配，以将指定的 topic 的所有分区移动到新的broker。此选项仅提供了一种便捷的方式，可以根据 tpoc 和目标 broker 列表生成分区重新分配计划。
2. --execute: 在此模式下，该工具基于用户提供的重新分配计划启动分区重新分配。（使用--reassignment-json-file选项）。这可以是由管理员制作的自定义重新分配计划，也可以是使用--generate选项提供的自定义重新分配计划。
3. --verify: 在此模式下，该工具将验证最近用 --execute 模式执行间的所有分区的重新分配状态。状态可以是成功完成，失败或正在进行



### 数据迁移

##### 脚本自动数据迁移

分区重新分配工具可用于将当前一组 broker 的一些 topic 移至新增的topic。这在扩展现有群集时通常很有用，因为将整个 topic 移动到新 broker 集比移动一个分区更容易。当这样做的时候，用户应该提供需要移动到新的 broker 集合的 topic 列表和新的目标broker列表。该工具然后会均匀分配新 broker 集中 topic 的所有分区。在此过程中，topic 的复制因子保持不变。实际上，所有输入 topic 的所有分区副本都将从旧的broker 组转移到新 broker中



```shell
# 生成 候选分配
bin/kafka-reassign-partitions.sh --zookeeper localhost:2181 --topics-to-move-json-file topics-to-move.json --broker-list "5,6" --generate

# 保存上面输出json 候选分配配置
# 执行 分配
bin/kafka-reassign-partitions.sh --zookeeper localhost:2181 --reassignment-json-file expand-cluster-reassignment.json --execute

# 验证 分配
bin/kafka-reassign-partitions.sh --zookeeper localhost:2181 --reassignment-json-file expand-cluster-reassignment.json --verify
```

##### 自定义数据迁移

将 脚本 生成的候选分配的json文件修改一下, 重新执行分配,和验证步骤

##### 下线 brokers

手动迁移broker对应的分区数据



为什么? 

分区重新分配工具不具备为下线 broker 自动生成重新分配计划的功能。因此，管理员必须自己整理重新分配计划，将托管在即将下线的 broker 上的所有分区的副本移动到其他 broker。这可能比较单调，因为重新分配需要确保所有副本不会从将下线的 broker 只转移到唯一的 broker。 为了使这一过程毫不费力，我们计划在未来为下线 broker 添加工具支持

##### 增加复制因子



修改`increase-replication-factor.json `文件配置即可

```shell
# 执行分配
bin/kafka-reassign-partitions.sh --zookeeper localhost:2181 --reassignment-json-file increase-replication-factor.json --execute

# 执行验证
bin/kafka-reassign-partitions.sh --zookeeper localhost:2181 --reassignment-json-file increase-replication-factor.json --verify

# 执行回滚, 保存分配前的配置, 重新执行分配

```

##### 限制数据迁移过程中的带宽使用

Kafka允许您设置复制流量的阈值，设置用于将副本从机器移动到另一台机器上的带宽上限。在重新平衡群集，引导新 broker 或添加或删除 broker 时，这非常有用，因为它限制了这些数据密集型操作对用户的影响。



```shell
# —throttle 50000000 限流
bin/kafka-reassign-partitions.sh --zookeeper myhost:2181--execute --reassignment-json-file bigger-cluster.json —throttle 50000000

# 当执行--verify选项并且重新分配完成时，脚本将确认节流阀已被移除
bin/kafka-reassign-partitions.sh --zookeeper localhost:2181  --verify --reassignment-json-file bigger-cluster.json	
```



在 `kafka-configs.sh` 验证分配配置

```shell
leader.replication.throttled.rate
  follower.replication.throttled.rate

leader.replication.throttled.replicas
  follower.replication.throttled.replicas
  
# 查看流量限制配置
bin/kafka-configs.sh --describe --zookeeper localhost:2181 --entity-type brokers

# 要查看节流副本的列表
bin/kafka-configs.sh --describe --zookeeper localhost:2181 --entity-type topics
```

##### 安全使用节流复制

1. 移除节流阀
2. 确保复制速度必须大于写入速度

### 数据持久化

Kafka总是立即将所有数据写入文件系统，并支持配置刷新策略的功能，该策略控制何时将数据从OS缓存中强制刷新到磁盘上。该刷新策略可以控制在一段时间之后或者在写入一定数量的消息之后把数据持久化到磁盘。

Kafka最总必须调用fsync指令才能知道数据已经被刷新。当从任何不被fsync所知的日志段崩溃中恢复时，Kafka将通过每个消息的CRC来检查其完整性，并且在启动时执行的恢复过程中会重建相应的偏移量索引文件。

请注意，Kafka中的持久性并不需要将数据同步到磁盘，因为失败的节点将始终从其副本中恢复。



我们建议**使用完全禁用应用程序fsync的默认刷新设置**。这意味着依靠操作系统和Kafka自己的后台完成的刷新操作。这种设置对大多数用途是最好的选择：无需调整，巨大的吞吐量和延时，以及完全的恢复保证。我们认为通过复制提供的保证比同步到本地磁盘更好，但是一些偏执的人仍然可能喜欢让OS，Kafka和应用程序级别的fsync策略都得到支持。



### 集群升级迁移





## 深入实现原理

### 使用过程

```mermaid
sequenceDiagram
title: kafka使用过程
participant  producer as 生产者业务系统
participant  server as  kafka cluster server 
participant  consumer as  消费者业务系统

producer ->> +server: 生产消息
server->> server: 写入消息
alt 写入成功
server-->> producer: ack 成功
else 写入失败
server -->> producer: ack 失败
producer ->> producer:  写入失败处理
else 超时
loop 重试失败
producer->>server: 重试
end
server-->>-producer: 发送成功
end

consumer->>+server: 消费消息
consumer->>consumer: 消息处理,幂等
consumer->>server: 提交offset
server-->>-consumer: 提交结果
alt 提交失败
consumer-->consumer: 跳过,重复消费,幂等处理
else 提交成功
note over consumer: 等待处理下一条消息
end



```

### kafka支持的几种语义

1. at most once
   最多一次, 消息会丢失
2. at least once
   最少一次, 消息会重复
3. exactly once
   精确一次

### exactly once

exactly once 的目的, 同一个消息只被消费一次

语义保证的粒度

1. Idempotent Producer：Exactly-once，in-order，delivery per partition；
   幂等生产者, 每分区准确一次,按顺序发送; 实现方式：类似于TCP，发送到kafka的每批消息将包含一个序列号，该序列号用于重复数据的删除。与TCP不同的是，TCP只能在transient in-memory中提供保证。而序列号将被持久化存储到topic中，因此即使leader replica失败，接管的任何其他broker也能感知到消息是否重复。而且这种机制开销相当低，只需在每批消息中添加几个额外的字段
2. Transactions：Atomic writes across partitions
   事务:  跨分区原子写入; 在一个事务内的消息要么全部可见，要么全都不可见。consumers必须被配置成，可跳过未提交的消息。
3. Exactly-Once stream processing across read-process-write tasks；
   准确一次的流处理, 因为幂等生产者和事务消息给流式处理提供了基础

#### 幂等性exactly once

[kafka幂等性exactly once 实现原理](http://matt33.com/2018/10/24/kafka-idempotent/)

在单个会话, 单个partition内, 生产者做到生产数据不丢不重, 消费者消费做到不丢不重, 这就是幂等性exactly once



业务系统如何通过幂等性exactly once 达到业务的exactly once?  

生产者投递消息不丢不重, 消息被消费至少一次, 但是对消息做幂等处理, 重复消费相同的消息对系统状态不会产生偏差, 这样达到exactly once

##### Producer 幂等性

Producer 的幂等性指的是当在**单个会话内**, **单个partition内**, 发送**同一条消息**时，数据在 Server 端只会被**持久化一次**，数据不丟不重

有条件

- 只能保证 Producer 在**单个会话内**不丟不重 如果 Producer 出现意外挂掉再重启是无法保证的（幂等性情况下，是无法获取之前的状态信息，因此是无法做到跨会话级别的不丢不重）
- 幂等性**不能跨多个 Topic-Partition**，只能保证单个 partition 内的幂等性，当涉及多个 Topic-Partition 时，这中间的状态并没有同步

##### 幂等性是来解决什么问题的？

- 数据不丢
- 数据不重

##### kafka如何实现数据不丢? 

在 0.11.0 之前，Kafka 通过 Producer 端和 Server 端的相关配置可以做到**数据不丢**，也就是 at least once，但是在一些情况下，可能会导致数据重复，比如：网络请求延迟等导致的重试操作，在发送请求重试时 Server 端并不知道这条请求是否已经处理（没有记录之前的状态信息），所以就会有可能导致数据请求的重复发送，这是 Kafka 自身的机制（异常时请求重试机制）导致的数据重复。

##### kafka如何实现数据不重? 

数据重复, 那怎么知道数据是重复的

###### 如何定义重复消息? 

常用的手段是通过 **唯一键/唯一 id** 来判断，这时候系统一般是需要缓存已经处理的唯一键记录，这样才能更有效率地判断一条数据是不是重复；

###### 唯一键应该选择什么粒度?

核心思路: 分而治之

对于 Kafka 而言，这里的解决方案就是在分区的维度上去做，重复数据的判断让 partition 的 leader 去判断处理，前提是 Produce 请求需要把唯一键值告诉 leader；

###### 分区粒度实现唯一键会不会有其他问题？

这里需要考虑的问题是当一个 Partition 有来自多个 client 写入的情况，这些 client 之间是很难做到使用同一个唯一键（一个是它们之间很难做到唯一键的实时感知，另一个是这样实现是否有必要）。而如果系统在实现时做到了 **client + partition** 粒度，这样实现的好处是每个 client 都是完全独立的（它们之间不需要有任何的联系，这是非常大的优点），只是在 Server 端对不同的 client 做好相应的区分即可，当然同一个 client 在处理多个 Topic-Partition 时是完全可以使用同一个 PID 的

##### kafka如何实现幂等性exactly once? 

at least once + 消费幂等 =  exactly once

1. PID（Producer ID），用来标识每个 producer client；
2. sequence numbers，client 发送的每条消息都会带相应的 sequence number，Server 端就是根据这个值来判断数据是否重复。

###### PID怎么生成?

每个 Producer 在初始化时都会被分配一个唯一的 PID，这个 PID 对应用是透明的，完全没有暴露给用户。对于一个给定的 PID，sequence number 将会从0开始自增，每个 Topic-Partition 都会有一个独立的 sequence number。Producer 在发送数据时，将会给每条 msg 标识一个 sequence number，Server 也就是通过这个来验证数据是否重复。这里的 **PID 是全局唯一**的，Producer 故障后重新启动后会被分配一个新的 PID，这也是幂等性无法做到跨会话的一个原因。

```mermaid
sequenceDiagram
title: Producer ID申请流程
participant p as  Producer
participant k as Kafka Server
participant z as zookeeper
p->>k: 1. 申请PID
k->>z: 2. 获取/latest_producer_id_block值
alt 值不存在
note over k: 3.新建latest_producer_id_block的zookeeper值
note over k: 4.直接从0开始分配ProducerID
else 值存在
note over k: 5.latest_producer_id_block不能超过long的最大值<br/>超过报异常
end
k->>z: 6.更新latest_producer_id_block
z-->>k: 7.更新结果
note right of z: latest_producer_id_block带有版本号,<br/>使用CAS更新策略
alt 写入成功
k-->>p: 8.分配PID
else 写入失败
note over k: 9.重试
end

```

###### sequence number怎么生成? 

再有了 PID 之后，在 PID + Topic-Partition 级别上添加一个 sequence numbers 信息，就可以实现 Producer 的幂等性了。ProducerBatch 也提供了一个 `setProducerState()` 方法，它可以给一个 batch 添加一些 meta 信息（pid、baseSequence、isTransactional），这些信息是会伴随着 ProduceRequest 发到 Server 端，Server 端也正是通过这些 meta 来做相应的判断

###### 幂等性实现整体流程





#### 事务性exactly once

提供消息事务的ACID性质, 这样达到exactly once

kafka事务语义的保证

##### Producer角度

1. **跨会话的幂等性写入**
   即使中间故障,恢复之后依然可以保证幂等性

2. **跨会话的事务恢复**

   如果broker挂了, 下一个broker启动时, 依然保证事务是被提交或者中断的

3. **跨多个Topic-Partition幂等写入**
   要么全部成功, 要么全部失败

##### Consumer角度

Consumer很难保证一个已经被commit的事务的消息被全部消费到

为什么会造成这种情况? 

1. 对于 compacted topic，在一个事务中写入的数据可能会被新的值覆盖
2. 一个事务内的数据,可能会跨越多个log segment, 如果旧的log segment由于数据过期而被清除掉,那么这部分的数据就无法被消费到
3. Consumer 在消费时可以通过 seek 机制，随机从一个位置开始消费，这也会导致一个事务内的部分数据无法消费
4. Consumer 可能没有订阅这个事务涉及的全部 Partition

kafka事务性exactly once 语义 保证下面的情况: 

1. Atomic writes across multiple partitions.
   跨多个partition原子写入
2. All messages in a transaction are made visible together, or none are.
   事务内的全部数据要么全部可见,要么全部不可见
3. Consumers must be configured to skip uncommitted messages.
   消费端必须被配置成跳过没有被提交的消息

#### 为什么会出现事务性exactly once? 

为了解决幂等性exactly once没有解决的问题

跨多个topic-partition时, 容易出现部分topic写入成功, 部分topic写入失败



#### 总结

幂等性exactly once

- 单topic场景

事务性exactly once

- 跨会话幂等写入幂等写入
- 无法保证写入的消息全部被消费



## 资料

[kafka官方中文文档](https://kafka.apachecn.org/documentation.html)

[kafka官方文档-最新](https://kafka.apache.org/documentation/)

[kafka幂等性exactly once](http://matt33.com/2018/10/24/kafka-idempotent/#%E5%B9%82%E7%AD%89%E6%80%A7%E8%A6%81%E8%A7%A3%E5%86%B3%E7%9A%84%E9%97%AE%E9%A2%98)

[kafka 事务性exactly once 实现原理](http://matt33.com/2018/11/04/kafka-transaction/#%E4%BA%8B%E5%8A%A1%E6%80%A7%E5%AE%9E%E7%8E%B0%E7%9A%84%E5%85%B3%E9%94%AE)