package main

import "fmt"

func main() {
	// 定义数组, 数组是值类型
	//    1
	var arr1 [5]int
	fmt.Println(arr1)
	for i, v := range arr1 {
		fmt.Println(i, v)
	}

	for _, v := range arr1 {
		fmt.Println(v)
	}
	//    2
	arr2 := [3]int{1, 2, 3}
	fmt.Println(arr2)

	//    3
	arr3 := [...]int{1, 2, 3}
	println(3, "go")
	printArray(arr3)
	fmt.Println(arr3)

	printArrayWith(&arr3)
	fmt.Println(arr3)

	// 4
	arr4 := [3][4]int{}
	fmt.Println(arr4)

}

func printArray(arr [3]int) {
	arr[0] = 789
	for _, v := range arr {
		fmt.Print(v, " ")
	}
	println()
}

func printArrayWith(arr *[3]int) {
	arr[0] = 789
	for _, v := range arr {
		fmt.Print(v, " ")
	}
	println()

}
