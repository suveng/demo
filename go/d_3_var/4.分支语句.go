package main

import (
	"fmt"
	"io/ioutil"
)

func main() {
	// 条件语句
	var filename = "d_3_var/test.txt"
	file, err := ioutil.ReadFile(filename)
	if err != nil {
		panic("文件读取失败")
	}

	fmt.Printf("%s \n", file)

	// switch
	var score = 123
	println(switchScore(score))

}

func switchScore(score int) bool {
	var result = false
	switch {
	case score < 60:
		result = false
	default:
		panic("error")
	}

	return result
}
