package main

// 2.内建变量类型

func main() {
	// 1. bool
	var b bool = false
	println(b)
	// 2. string
	var s string = "hello"
	println(s)

	// 3.int uint(8,16,32,64), uintptr指针
	var i int64 = 32
	println(i)
	// 4. byte, rune(char类型)
	var by byte = 'c'
	println(by)

	// 5. float32 float64 complex64 complex128 复数,实部和虚部
	var f1 float32 = 32.32
	println(f1)

}
