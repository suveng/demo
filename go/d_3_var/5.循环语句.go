package main

func main() {

	// 有限 for
	for i := 0; i < 100; i++ {
		println(i)
	}

	// 无限for
	var i = 0
	for {
		println("forever", i)
		i++
		if i > 100 {
			break
		}
	}
}
