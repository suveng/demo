package main

import "fmt"

func main() {
	// map结构
	m := map[string]string{
		"k1": "v1",
		"k2": "v2",
	}
	fmt.Println(m)

	// 遍历
	for v, s := range m {
		fmt.Println(v, s)
	}

}
