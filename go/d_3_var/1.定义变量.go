package main

// 5.定义包变量
var v7 = "string var"
var v8 = 87

func main() {
	// 定义变量
	// 1. 方法内部
	var v1 int64 = 21
	println(v1)

	// 2.定义多个变量
	var v2, v3 int64 = 32, 32
	println(v2)
	println(v3)

	// 3. 变量类型推断
	var v4, v5 = "hello", 23
	println(v4)
	println(v5)

	// 4. 省略var

	v6 := "省略"
	println(v6)

	// 5.定义包变量
	println(v7)
	println(v8)

}
