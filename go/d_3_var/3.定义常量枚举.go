package main

const FILENAME = "hello"

func main() {
	// 常量 const
	println(FILENAME)

	//    枚举类型
	const (
		e1 = 1
		e2 = 2
		e3 = 3
		e4 = 4
	)
	println(e1, e2, e3, e4)

	// 枚举iota自增 语法糖
	const (
		v1 = iota
		v2
		v3
		v4
	)
	println(v1, v2, v3, v4)

	// iota运算
	const (
		b = 1 << (iota * 10)
		kb
		mb
		gb
		tb
		pb
	)

	println(b, kb, mb, gb, tb, pb)

}
