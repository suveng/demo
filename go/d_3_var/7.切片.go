package main

import "fmt"

func main() {

	// 切片 slice, 数组的视图 view
	arr := [...]int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}
	fmt.Println(arr)

	// 1
	ints := arr[2:6]
	fmt.Println(ints)

	// 2
	fmt.Println(arr[:6])

	// 3
	fmt.Println(arr[2:])

	// 4
	fmt.Println(arr[:])

	update(ints)

	var arr5 []int
	for i := 0; i < 100; i++ {
		arr5 = append(arr5, i)
		fmt.Println(arr5)
	}
	fmt.Println(arr5)

}

func update(ints []int) {
	ints[2] = 100
}
