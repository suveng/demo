package main

import "fmt"

func main() {

	account := Account{
		username:   "hello",
		password:   "hello",
		verifyCode: "1234",
	}

	fmt.Println(account)
}

type Account struct {
	username   string
	password   string
	verifyCode string
}
