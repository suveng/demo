package my.suveng.guava;

import com.google.common.cache.Cache;
import com.google.common.cache.LoadingCache;

/**
 * @author suwenguang
 * created since 2020/9/26
 */
public class GuavaCache {

	/**
	 * 使用 {@link com.google.common.cache.CacheLoader} 创建的 guava cache
	 */
	private LoadingCache<String,Object> cacheLoader;

	/**
	 * 使用 {@link Cache} 创建的 guava cache
	 */
	private Cache<String,Object> callableCache;


	/**
	 * guava cache 使用
	 */
	public GuavaCache() {
		init();
	}

	/**
	 * 初始化guava cache
	 * {@link com.google.common.cache.CacheLoader}
	 * {@link java.util.concurrent.Callable}
	 */
	private void init(){
	//	1. cache loader
		 cacheLoader = cacheLoaderCreateCache();
	//	2. callable
		callableCache = callableCreateCache();
	}

	/**
	 * 使用 {@link java.util.concurrent.Callable} 创建 guavaCache
	 * @return {@link Cache}
	 */
	private Cache<String, Object> callableCreateCache() {
		return null;
	}

	/**
	 * 使用 cacheLoader 方式创建一个 guavaCache
	 * @return {@link LoadingCache} guavaCache
	 */
	private LoadingCache<String, Object> cacheLoaderCreateCache() {
		return null;
	}

	/**
	 * 增加cache一条记录
	 * @return 增加的记录
	 */
	private Object add(){
		return  null;
	}


	/**
	 * 根据key删除对应的记录
	 * @param key 索引
	 * @return 被删除的记录, 必须删除成功, 如果删除失败, 或者异常, 进入重试队列
	 */
	private Object delete(String key){
		return null;
	}

	/**
	 * 根据key获取cache记录
	 * @param key 关键
	 * @return cache的记录,如果没有, 则返回空
	 */
	private Object get(String key){
		return null;
	}


	/**
	 * 根据key更新cache记录ª
	 * @param key 索引
	 * @param obj 待更新的记录
	 * @return obj 旧的记录
	 */
	private Object update(String key , Object obj){
		return null;
	}
}
