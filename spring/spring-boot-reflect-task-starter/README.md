# 基于redisson的轻量级分布式任务调度

## 接入

接入的应用必须满足以下条件
1. springboot 2.0以上
2. redis5.0以上
3. mysql5.7以上

## 特性

1. 方法级别, 基于spring bean 的方法级别的任务调度
2. 通知策略, 基于任务记录的状态定制通知策略
3. 通知方式, 支持钉钉群聊机器人
4. 失败策略, 支持重试,
5. 状态变更路径, 在扩展属性里面



