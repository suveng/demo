/*
 * Cainiao.com Inc.
 * Copyright (c) 2013-2021 All Rights Reserved.
 */

package com.gitee.suveng.springboot.reflect.task.config;

import com.gitee.suveng.springboot.reflect.task.model.enums.TaskEvent;
import com.gitee.suveng.springboot.reflect.task.model.enums.TaskStates;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachineFactory;
import org.springframework.statemachine.config.StateMachineConfigurerAdapter;

/**
 * @author suwenguang
 * @since 2021-05-25 18:00
 */
@Configuration
@EnableStateMachineFactory
public class TaskStateMachineFactoryConfig extends StateMachineConfigurerAdapter<TaskStates, TaskEvent> {

}

