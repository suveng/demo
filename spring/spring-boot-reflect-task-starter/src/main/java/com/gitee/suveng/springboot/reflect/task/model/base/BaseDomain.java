

package com.gitee.suveng.springboot.reflect.task.model.base;

import java.util.Date;

import lombok.Data;

/**
 * @author suwenguang
 * @since 2021-05-21 15:35
 */
@Data
public abstract class BaseDomain {
	/**
	 * 扩展
	 */
	private String properties;

	/**
	 * 创建时间
	 */
	private Date createdTime;

	/**
	 * 修改时间
	 */
	private Date modifiedTime;
}
