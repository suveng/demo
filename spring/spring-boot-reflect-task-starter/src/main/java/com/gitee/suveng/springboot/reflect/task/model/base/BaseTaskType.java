

package com.gitee.suveng.springboot.reflect.task.model.base;

/**
 * 任务类型实现模型, 执行任务需要的参数
 *
 * @author suwenguang
 * @since 2021-05-21 11:09
 */
public interface BaseTaskType {
}
