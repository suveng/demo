

package com.gitee.suveng.springboot.reflect.task.model.core;

import java.util.List;
import java.util.Map;

import com.gitee.suveng.springboot.reflect.task.model.base.BaseTaskType;
import lombok.Data;

/**
 * Java反射模型, 需要的参数
 *
 * @author suwenguang
 * @since 2021-05-21 11:10
 */
@Data
public class JavaReflect implements BaseTaskType {
	/**
	 * 类名
	 */
	private String className;

	/**
	 * 方法名
	 */
	private String methodName;

	/**
	 * 参数map
	 */
	private List<Map<Class<?>, ?>> args;
}
