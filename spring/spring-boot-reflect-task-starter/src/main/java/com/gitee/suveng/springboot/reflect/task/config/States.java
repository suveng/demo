
package com.gitee.suveng.springboot.reflect.task.config;

/**
 * @author suwenguang
 * @since 2021-05-25 13:58
 */
public enum States {
	SI, S1, S2
}
