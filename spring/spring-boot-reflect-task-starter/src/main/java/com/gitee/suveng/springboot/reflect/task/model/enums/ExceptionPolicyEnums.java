

package com.gitee.suveng.springboot.reflect.task.model.enums;

import com.gitee.suveng.springboot.reflect.task.model.base.ExceptionPolicy;
import com.gitee.suveng.springboot.reflect.task.model.policy.exceptions.CallbackPolicy;
import com.gitee.suveng.springboot.reflect.task.model.policy.exceptions.ComplexPolicy;
import com.gitee.suveng.springboot.reflect.task.model.policy.exceptions.RetryPolicy;

/**
 * 任务异常策略
 *
 * @author suwenguang
 * @since 2021-05-21 13:52
 */
public enum ExceptionPolicyEnums {
	/**
	 * 不做任何事情
	 */
	NONE(null),

	/**
	 * 重试
	 */
	RETRY(RetryPolicy.class),

	/**
	 * 回调钩子
	 */
	CALLBACK(CallbackPolicy.class),

	/**
	 * 复合策略
	 */
	COMPLEX(ComplexPolicy.class);

	private final Class<? extends ExceptionPolicy> policy;

	/**
	 * Getter method for property policy.
	 *
	 * @return property value of policy
	 */
	public Class<? extends ExceptionPolicy> getPolicy() {
		return policy;
	}

	ExceptionPolicyEnums(
		Class<? extends ExceptionPolicy> policy) {
		this.policy = policy;
	}
}
