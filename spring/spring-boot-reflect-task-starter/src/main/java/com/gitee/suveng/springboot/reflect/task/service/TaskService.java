

package com.gitee.suveng.springboot.reflect.task.service;

import java.util.List;

import com.gitee.suveng.springboot.reflect.task.model.core.Task;

/**
 * 任务模型对外API
 *
 * @author suwenguang
 * @since 2021-05-21 15:02
 */
public interface TaskService {

	/**
	 * 新增一个task
	 *
	 * @param task {@link Task}
	 * @return {@link Task#getId()}
	 */
	String add(Task task);

	/**
	 * 删除一个task
	 *
	 * @param task {@link Task}
	 * @return true: 成功; false:失败
	 */
	boolean delete(Task task);

	/**
	 * 暂停一个task
	 *
	 * @param task task
	 * @return true: 成功; false: 失败
	 */
	boolean disable(Task task);

	/**
	 * 开启一个task
	 *
	 * @param task {@link Task}
	 * @return true: 成功; false: 失败
	 */
	boolean enable(Task task);

	/**
	 * 执行一次task
	 *
	 * @param task {@link Task}
	 * @return true: 成功; false:失败
	 */
	boolean runOnce(Task task);

	/**
	 * 查询task列表
	 *
	 * @param task 查询条件
	 * @return List
	 */
	List<Task> query(Task task);

}
