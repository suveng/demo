

package com.gitee.suveng.springboot.reflect.task.model.policy.exceptions;

import com.gitee.suveng.springboot.reflect.task.model.base.ExceptionPolicy;

/**
 * 异常策略-重试
 *
 * @author suwenguang
 * @since 2021-05-21 14:11
 */
public class RetryPolicy implements ExceptionPolicy {
}
