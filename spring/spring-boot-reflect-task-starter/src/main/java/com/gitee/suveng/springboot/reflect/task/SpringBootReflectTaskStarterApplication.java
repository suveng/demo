package com.gitee.suveng.springboot.reflect.task;

import cn.hutool.extra.spring.SpringUtil;
import com.gitee.suveng.springboot.reflect.task.service.StateMachineService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class SpringBootReflectTaskStarterApplication {

	@Bean
	public SpringUtil springUtil(){
		return  new SpringUtil();
	}

	public static void main(String[] args) {
		SpringApplication.run(SpringBootReflectTaskStarterApplication.class, args);
		StateMachineService bean = SpringUtil.getBean(StateMachineService.class);
		bean.change();
		bean.stringStateChange();
	}

}
