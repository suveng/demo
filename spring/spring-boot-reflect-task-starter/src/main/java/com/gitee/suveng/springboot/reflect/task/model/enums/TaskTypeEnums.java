

package com.gitee.suveng.springboot.reflect.task.model.enums;

import com.gitee.suveng.springboot.reflect.task.model.core.JavaReflect;
import com.gitee.suveng.springboot.reflect.task.model.base.BaseTaskType;

/**
 * 任务类型枚举
 *
 * @author suwenguang
 * @since 2021-05-21 11:07
 */
public enum TaskTypeEnums {
	// java 反射
	JAVA(JavaReflect.class);

	private final Class<? extends BaseTaskType> impl;

	/**
	 * Getter method for property impl.
	 *
	 * @return property value of impl
	 */
	public Class<? extends BaseTaskType> getImpl() {
		return impl;
	}

	TaskTypeEnums(Class<? extends BaseTaskType> impl) {
		this.impl = impl;
	}
}
