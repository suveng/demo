

package com.gitee.suveng.springboot.reflect.task.model.enums;

import com.gitee.suveng.springboot.reflect.task.service.NotifyTypeService;
import com.gitee.suveng.springboot.reflect.task.model.core.notify.DingDingGroupRobotNotifyType;
import com.gitee.suveng.springboot.reflect.task.model.core.notify.DingWorkMessageNotifyType;
import com.gitee.suveng.springboot.reflect.task.model.core.notify.EmailNotifyType;
import com.gitee.suveng.springboot.reflect.task.model.core.notify.PhoneNotifyType;

/**
 * 通知策略
 *
 * @author suwenguang
 * @since 2021-05-21 15:22
 */
public enum NotifyTypeEnums {
	//
	DING_DING_GROUP_ROBOT(DingDingGroupRobotNotifyType.class),
	DING_WORK_MESSAGE(DingWorkMessageNotifyType.class),
	PHONE(PhoneNotifyType.class),
	EMAIL(EmailNotifyType.class);

	private final Class<? extends NotifyTypeService> type;

	NotifyTypeEnums(Class<? extends NotifyTypeService> type) {
		this.type = type;
	}

	/**
	 * Getter method for property type.
	 *
	 * @return property value of type
	 */
	public Class<? extends NotifyTypeService> getType() {
		return type;
	}
}
