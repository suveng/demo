

package com.gitee.suveng.springboot.reflect.task.model.core;

import java.util.List;

import com.gitee.suveng.springboot.reflect.task.model.base.BaseDomain;
import com.gitee.suveng.springboot.reflect.task.model.enums.ExceptionPolicyEnums;
import com.gitee.suveng.springboot.reflect.task.model.enums.NotifyPolicyEnums;
import com.gitee.suveng.springboot.reflect.task.model.enums.TaskTypeEnums;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author suwenguang
 * @since 2021-05-21 11:00
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class Task extends BaseDomain {

	/**
	 * 唯一id
	 */
	private String id;

	/**
	 * 责任人
	 */
	private String userId;

	/**
	 * 名称
	 */
	private String name;

	/**
	 * 别名
	 */
	private String aliasName;

	/**
	 * 任务类型; 1. Java反射
	 */
	private TaskTypeEnums type;

	/**
	 * 任务类型值, 去 type 里面的类型,强转
	 */
	private Object typeValue;

	/**
	 * 任务描述
	 */
	private String description;

	/**
	 * 超时时间; 单位: 秒
	 */
	private Long executeOverTime;

	/**
	 * 启用标记; 1:启用,2不启用
	 */
	private Integer isEnable;

	/**
	 * 任务场景来源; 1.配置 2.通知 3.重试  4.其他, 动态添加
	 */
	private String sceneSource;


	/**
	 * 关联的通知方式配置
	 */
	private List<Notify> notifies;

	/**
	 * 异常策略
	 */
	private ExceptionPolicyEnums exceptionPolicy;

	/**
	 * 通知策略
	 */
	private NotifyPolicyEnums notifyPolicy;



}
