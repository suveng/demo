

package com.gitee.suveng.springboot.reflect.task.model.core;

import com.gitee.suveng.springboot.reflect.task.model.base.BaseDomain;
import com.gitee.suveng.springboot.reflect.task.model.enums.NotifyTypeEnums;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * 通知配置模型
 *
 * @author suwenguang
 * @since 2021-05-21 14:23
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class Notify extends BaseDomain {

	/**
	 * 唯一id
	 */
	private String id;

	/**
	 * 通知方式 1. 钉钉企业工作通知 2. 钉钉机器人 3. 手机短信 4. 邮件
	 */
	private NotifyTypeEnums notifyType;

	/**
	 * 通知方式对应的
	 */
	private Object notifyValue;

}
