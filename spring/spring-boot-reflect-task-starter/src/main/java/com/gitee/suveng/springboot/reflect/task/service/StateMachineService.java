/*
 * Cainiao.com Inc.
 * Copyright (c) 2013-2021 All Rights Reserved.
 */

package com.gitee.suveng.springboot.reflect.task.service;

import com.gitee.suveng.springboot.reflect.task.config.Events;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.statemachine.StateMachine;
import org.springframework.stereotype.Service;

/**
 * @author suwenguang
 * @since 2021-05-25 14:01
 */
@Service
public class StateMachineService {
	//@Autowired
	//private StateMachine<States, Events> stateMachine;

	@Autowired
	private StateMachine<String,String> stringStateMachine;

	public void change() {
		//stateMachine.sendEvent(Events.E1);
		//stateMachine.sendEvent(Events.E2);
	}

	public void stringStateChange() {
		stringStateMachine.sendEvent(Events.E1.name());
		stringStateMachine.sendEvent(Events.E2.name());
	}


}
