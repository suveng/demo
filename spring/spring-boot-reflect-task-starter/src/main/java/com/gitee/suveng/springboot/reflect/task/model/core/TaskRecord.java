

package com.gitee.suveng.springboot.reflect.task.model.core;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author suwenguang
 * @since 2021-05-21 16:05
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class TaskRecord extends Task{
}
