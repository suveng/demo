package demo.jackson;

import com.fasterxml.jackson.annotation.JsonFilter;

/**
 *
 * @author suwenguang
 **/
@JsonFilter("myFilter")
public interface MyFilter {
}
