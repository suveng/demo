package com.gitee.suveng.springbootcustomaopstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootCustomAopStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootCustomAopStarterApplication.class, args);
	}

}
