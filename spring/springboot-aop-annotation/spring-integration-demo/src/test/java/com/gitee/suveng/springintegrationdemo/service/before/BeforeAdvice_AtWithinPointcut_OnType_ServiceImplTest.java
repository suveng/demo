package com.gitee.suveng.springintegrationdemo.service.before;

import com.gitee.suveng.springintegrationdemo.SpringIntegrationDemoApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class BeforeAdvice_AtWithinPointcut_OnType_ServiceImplTest extends SpringIntegrationDemoApplicationTests {

	@Autowired
	BeforeAdvice_AtWithinPointcut_OnType_ServiceImpl service;

	@Test
	void one() {
		service.one();
	}
}
