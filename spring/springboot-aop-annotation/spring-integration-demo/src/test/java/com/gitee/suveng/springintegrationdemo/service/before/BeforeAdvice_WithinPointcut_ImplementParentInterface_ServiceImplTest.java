package com.gitee.suveng.springintegrationdemo.service.before;

import com.gitee.suveng.springintegrationdemo.SpringIntegrationDemoApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

class BeforeAdvice_WithinPointcut_ImplementParentInterface_ServiceImplTest
	extends SpringIntegrationDemoApplicationTests {

	@Autowired
	BeforeAdvice_WithinPointcut_ImplementParentInterface_ServiceImpl service;

	@Test
	void one() {
		service.one();
	}
}
