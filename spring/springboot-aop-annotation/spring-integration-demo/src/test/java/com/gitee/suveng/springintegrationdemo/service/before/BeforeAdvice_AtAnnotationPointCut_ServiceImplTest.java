package com.gitee.suveng.springintegrationdemo.service.before;

import com.gitee.suveng.springintegrationdemo.SpringIntegrationDemoApplicationTests;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;


public class BeforeAdvice_AtAnnotationPointCut_ServiceImplTest extends SpringIntegrationDemoApplicationTests {
	@Autowired
	BeforeAdvice_AtAnnotationPointCut_ServiceImpl service;


	@Test
	void one() {
		service.one();
	}

	@Test
	void two() {
	}

	@Test
	void three() {
	}

	@Test
	void four() {
	}
}
