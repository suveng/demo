package com.gitee.suveng.springintegrationdemo.service.before;

import com.gitee.suveng.springintegrationdemo.SpringIntegrationDemoApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;


public class BeforeAdvice_AtTargetPointcut_ServiceImplTest extends SpringIntegrationDemoApplicationTests {

	@Autowired
	BeforeAdvice_AtTargetPointcut_ServiceImpl service;

	@Test
	void testOne() {
		service.one();
	}

}
