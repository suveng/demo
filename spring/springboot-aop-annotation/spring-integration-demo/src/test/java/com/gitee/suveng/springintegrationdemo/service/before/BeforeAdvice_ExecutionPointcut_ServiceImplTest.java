package com.gitee.suveng.springintegrationdemo.service.before;

import com.gitee.suveng.springintegrationdemo.SpringIntegrationDemoApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class BeforeAdvice_ExecutionPointcut_ServiceImplTest extends SpringIntegrationDemoApplicationTests {

	@Autowired
	BeforeAdvice_ExecutionPointcut_ServiceImpl service;

	@Test
	void one() throws Exception {
		service.one();
	}
}
