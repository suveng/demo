package com.gitee.suveng.springintegrationdemo.service.before;

import com.gitee.suveng.springintegrationdemo.SpringIntegrationDemoApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

public class BeforeAdvice_TargetPointcut_ImplementParentInterface_ServiceImplTest
	extends SpringIntegrationDemoApplicationTests {

	@Autowired
	BeforeAdvice_TargetPointcut_ImplementParentInterface_ServiceImpl service;

	@Test
	void one() {
		service.one();
	}
}
