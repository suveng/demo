package com.gitee.suveng.springintegrationdemo.service.before;

import com.gitee.suveng.springintegrationdemo.SpringIntegrationDemoApplicationTests;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.*;

class BeforeAdvice_TargetPointcut_ExtendSubClass_ServiceImplTest extends SpringIntegrationDemoApplicationTests {

	@Autowired
	BeforeAdvice_TargetPointcut_ExtendSubClass_ServiceImpl service;

	@Test
	void one() {
		service.one();
	}
}
