
package com.gitee.suveng.springintegrationdemo.service.before;

import com.gitee.suveng.springintegrationdemo.annotation.AtTargetPointcut;
import org.springframework.stereotype.Service;

/**
 * 测试 at target 切点语法
 * <p>
 * 是否生效? 目前是不生效的, 详情查看单测
 *
 * @author suwenguang
 * @see BeforeAdvice_AtTargetPointcut_ServiceImplTest
 * @since 2021-05-17 14:10
 */
@Service
@AtTargetPointcut
public class BeforeAdvice_AtTargetPointcut_ServiceImpl {
	public void one() {
		System.out.println(this.getClass().getSimpleName() + "public void one()");
	}
}
