
package com.gitee.suveng.springintegrationdemo.abstract_class;

import com.gitee.suveng.springintegrationdemo.aspect.BeforeAspect;

/**
 * 用于测试aop target 切点语法
 * <p>
 * 验证父类配置了切点, 子类是否生效? 生效
 *
 * @author suwenguang
 * @see BeforeAspect#targetUsageWithExtendParentClass()
 * @since 2021-05-19 18:26
 */
public class TargetSubClass extends TargetParentClass {
}
