
package com.gitee.suveng.springintegrationdemo.abstract_interface;

/**
 * 是 {@link TargetParentInterface} 的子类
 *
 * @author suwenguang
 * @since 2021-05-17 16:28
 */
public interface TargetSubInterface extends TargetParentInterface {
}
