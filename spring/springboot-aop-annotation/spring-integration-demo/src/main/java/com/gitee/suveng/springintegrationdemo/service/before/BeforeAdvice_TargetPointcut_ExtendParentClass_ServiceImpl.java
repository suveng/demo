
package com.gitee.suveng.springintegrationdemo.service.before;

import com.gitee.suveng.springintegrationdemo.abstract_class.TargetParentClass;
import org.springframework.stereotype.Service;

/**
 * 测试 target 切点语法; 继承父类方式
 * <p>
 * 是否生效? 生效
 *
 * @author suwenguang
 * @see BeforeAdvice_TargetPointcut_ExtendParentClass_ServiceImplTest
 * @since 2021-05-17 14:10
 */
@Service
public class BeforeAdvice_TargetPointcut_ExtendParentClass_ServiceImpl extends TargetParentClass {
	public void one() {
		System.out.println(this.getClass().getSimpleName() + "public void one()");
	}
}
