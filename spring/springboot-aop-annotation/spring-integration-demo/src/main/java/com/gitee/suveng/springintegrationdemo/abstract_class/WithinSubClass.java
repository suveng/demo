
package com.gitee.suveng.springintegrationdemo.abstract_class;

import com.gitee.suveng.springintegrationdemo.aspect.BeforeAspect;

/**
 * 用于测试 aop within 切点语法
 *
 * @author suwenguang
 * @see BeforeAspect#withinUsageWithExtendParentClass() 查看配置
 * @since 2021-05-19 18:26
 */
public class WithinSubClass extends WithinParentClass {
}
