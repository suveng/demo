
package com.gitee.suveng.springintegrationdemo.service.before;

import com.gitee.suveng.springintegrationdemo.abstract_class.TargetParentClass;
import com.gitee.suveng.springintegrationdemo.abstract_class.WithinParentClass;
import org.springframework.stereotype.Service;

/**
 * 测试 within 切点语法
 * <p>
 * 是否生效? 生效
 *
 * @author suwenguang
 * @see BeforeAdvice_WithinPointcut_ExtendParentClass_ServiceImplTest
 * @since 2021-05-17 14:10
 */
@Service
public class BeforeAdvice_WithinPointcut_ExtendParentClass_ServiceImpl extends WithinParentClass {

	public void one() {
		System.out.println(this.getClass().getSimpleName() + "public void one()");
	}

	public String two() {
		String result = this.getClass().getSimpleName() + "public String two()";
		System.out.println(result);
		return result;
	}

	public String three(String p1) {
		String result = this.getClass().getSimpleName() + "public String three(String p1)";
		System.out.println(result);
		return result;
	}

	public String four(String p1, Object p2) {
		String result = this.getClass().getSimpleName() + "public String four(String p1, Object p2)";
		System.out.println(result);
		return result;
	}

	private String five(String p1, Object p2) {
		String result = "private String five(String p1, Object p2)";
		System.out.println(result);
		return result;
	}
}
