
package com.gitee.suveng.springintegrationdemo.abstract_class;

import com.gitee.suveng.springintegrationdemo.aspect.BeforeAspect;

/**
 * 用于验证 aop within 切点语法
 *
 * @see BeforeAspect#withinUsageWithExtendParentClass()
 * @author suwenguang
 * @since 2021-05-19 18:26
 */
public class WithinParentClass {
}
