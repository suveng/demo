
package com.gitee.suveng.springintegrationdemo.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

/**
 * @author suwenguang
 * @since 2021-05-17 13:54
 */
@Aspect
@Component
@Slf4j
public class BeforeAspect {

	/**
	 * execution 切点语法
	 * <p>
	 * 任意包下的包含ServiceImpl的类的任意public方法
	 * <p>
	 * execution 语法, [访问属性] [返回值] [包] [类] [方法] [参数] [异常]
	 * <p>
	 * '*' 代表任意, '..' 表示 当前以及子包, '+' 表示父类及子类
	 */
	@Before(value = "execution(public * *..*ServiceImpl*.*(..) throws Exception)")
	public void executionUseage() {
		System.out.println("before aop execution : do something");
	}

	/**
	 * at annotation 切点语法
	 */
	@Before(value = "@annotation(com.gitee.suveng.springintegrationdemo.annotation.AtAnnotation)")
	public void atAnnotationUsage() {
		System.out.println("before aop at_annotation : do something");
	}

	/**
	 * at target 切点语法 target指向注解
	 * <p>
	 * 目前不行
	 */
	//@Before(value = "@target(com.gitee.suveng.springintegrationdemo.annotation.AtTargetPointCut)")
	public void atTargetUsageWithAnnotation() {
		System.out.println("before aop at target aop on annotation : do something");
	}

	/**
	 * within 切点语法
	 */
	@Before(value = "@within(com.gitee.suveng.springintegrationdemo.annotation.AtWithinPointcut)")
	public void atWithinUsageWithAnnotation() {
		System.out.println("before aop at within on annotation : do something");
	}

	/**
	 * target 切点语法 target 指向父接口
	 */
	@Before(value = "target(com.gitee.suveng.springintegrationdemo.abstract_interface.TargetParentInterface+)")
	public void targetUsageWithImplementParentInterface() {
		System.out.println("before aop target on parent interface : do something");
	}

	/**
	 * target 切点语法 target 指向父类
	 */
	@Before(value = "target(com.gitee.suveng.springintegrationdemo.abstract_class.TargetParentClass+)")
	public void targetUsageWithExtendParentClass() {
		System.out.println("before aop target on parent class : do something");
	}

	/**
	 * within 切点语法 within 指向父接口
	 */
	@Before(value = "within(com.gitee.suveng.springintegrationdemo.abstract_interface.WithinParentInterface+)")
	public void withinUsageWithParentInterface() {
		System.out.println("before aop within on parent interface : do something");
	}

	/**
	 * within 切点语法 within 指向父类
	 */
	@Before(value = "within(com.gitee.suveng.springintegrationdemo.abstract_class.WithinParentClass+)")
	public void withinUsageWithExtendParentClass() {
		System.out.println("before aop within on parent class : do something");
	}

}
