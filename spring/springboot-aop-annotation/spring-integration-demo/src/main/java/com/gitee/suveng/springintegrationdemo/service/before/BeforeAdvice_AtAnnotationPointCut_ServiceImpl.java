
package com.gitee.suveng.springintegrationdemo.service.before;

import com.gitee.suveng.springintegrationdemo.annotation.AtAnnotation;
import org.springframework.stereotype.Service;

/**
 * 测试 annotation 切点语法, @annotation 只会作用于方法
 * <p>
 * 是否生效? 生效
 *
 * @author suwenguang
 * @see BeforeAdvice_AtAnnotationPointCut_ServiceImplTest
 * @since 2021-05-17 14:10
 */
@Service
public class BeforeAdvice_AtAnnotationPointCut_ServiceImpl {

	@AtAnnotation
	public void one() {
		System.out.println(this.getClass().getSimpleName() + "public void one()");
	}

	@AtAnnotation
	public String two() {
		String result = this.getClass().getSimpleName() + "public String two()";
		System.out.println(result);
		return result;
	}

	@AtAnnotation
	public String three(String p1) {
		String result = this.getClass().getSimpleName() + "public String three(String p1)";
		System.out.println(result);
		return result;
	}

	@AtAnnotation
	public String four(String p1, Object p2) {
		String result = this.getClass().getSimpleName() + "public String four(String p1, Object p2)";
		System.out.println(result);
		return result;
	}

	@AtAnnotation
	private String five(String p1, Object p2) {
		String result = "private String five(String p1, Object p2)";
		System.out.println(result);
		return result;
	}

}
