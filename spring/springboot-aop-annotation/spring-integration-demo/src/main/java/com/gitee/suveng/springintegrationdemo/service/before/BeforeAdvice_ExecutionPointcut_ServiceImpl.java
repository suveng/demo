
package com.gitee.suveng.springintegrationdemo.service.before;

import org.springframework.stereotype.Service;

/**
 * 测试 execution 切点语法
 * <p>
 * 是否生效? 生效
 *
 * @author suwenguang
 * @see BeforeAdvice_ExecutionPointcut_ServiceImplTest
 * @since 2021-05-17 14:10
 */
@Service
public class BeforeAdvice_ExecutionPointcut_ServiceImpl {
	public void one() throws Exception {
		System.out.println(this.getClass().getSimpleName() + "public void one()");
	}
}
