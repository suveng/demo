
package com.gitee.suveng.springintegrationdemo.abstract_class;

import com.gitee.suveng.springintegrationdemo.aspect.BeforeAspect;

/**
 * 用于测试 aop target 切点语法
 *
 * @author suwenguang
 * @see BeforeAspect#targetUsageWithExtendParentClass() 查看aop配置
 * @since 2021-05-19 18:26
 */
public class TargetParentClass {
}
