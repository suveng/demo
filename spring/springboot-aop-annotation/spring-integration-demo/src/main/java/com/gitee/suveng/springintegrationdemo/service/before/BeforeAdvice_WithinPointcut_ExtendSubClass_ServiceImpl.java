
package com.gitee.suveng.springintegrationdemo.service.before;

import com.gitee.suveng.springintegrationdemo.abstract_class.TargetSubClass;
import com.gitee.suveng.springintegrationdemo.abstract_class.WithinSubClass;
import org.springframework.stereotype.Service;

/**
 * 测试 within 切点语法, 子类继承父类, 切点是否生效
 * <p>
 * 是否生效? 生效
 *
 * @author suwenguang
 * @see BeforeAdvice_WithinPointcut_ExtendSubClass_ServiceImplTest
 * @since 2021-05-17 14:10
 */
@Service
public class BeforeAdvice_WithinPointcut_ExtendSubClass_ServiceImpl extends WithinSubClass {

	public void one() {
		System.out.println(this.getClass().getSimpleName() + "public void one()");
	}
}
