
package com.gitee.suveng.springintegrationdemo.service.before;

import com.gitee.suveng.springintegrationdemo.abstract_interface.TargetParentInterface;
import org.springframework.stereotype.Service;

/**
 * 测试 target 切点语法; 实现父接口方式
 * <p>
 * 是否生效?  生效
 *
 * @author suwenguang
 * @see BeforeAdvice_TargetPointcut_ImplementParentInterface_ServiceImplTest
 * @since 2021-05-17 14:10
 */
@Service
public class BeforeAdvice_TargetPointcut_ImplementParentInterface_ServiceImpl implements TargetParentInterface {
	public void one() {
		System.out.println(this.getClass().getSimpleName() + "public void one()");
	}
}
