
package com.gitee.suveng.springintegrationdemo.service.before;

import com.gitee.suveng.springintegrationdemo.abstract_class.TargetSubClass;
import org.springframework.stereotype.Service;

/**
 * 测试 target 切点语法; 继承子类方式
 * <p>
 * 是否生效? 生效
 *
 * @author suwenguang
 * @see BeforeAdvice_TargetPointcut_ExtendSubClass_ServiceImplTest
 * @since 2021-05-17 14:10
 */
@Service
public class BeforeAdvice_TargetPointcut_ExtendSubClass_ServiceImpl extends TargetSubClass {
	public void one() {
		System.out.println(this.getClass().getSimpleName() + "public void one()");
	}
}
