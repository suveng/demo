# 基于注解的spring aop


## 目的

记录用注解的方式声明切面的入口

## 用途

我经常写一些开箱即用的spring boot starter 的组件
当这些开箱即用的组件需要用到aop的时候, 你肯定不希望写死 java 包的路径,
而是希望让集成你的starter的人能够自主判断在哪里使用你的组件

所以这里的demo就是其中的实现方式
> 扫描全路径下的spring component, 并且打上了你的自定义注解的地方, 就是你的starter组件的入口

## demo 描述

1. 编写基于注解的aop的starter
2. 编写简单的spring demo工程, 并引入上述引入starter
3. 编写测试用例


### 1. 编写基于注解的aop的starter


### 2. 编写简单的spring demo工程, 并引入上述引入starter


### 3. 编写测试用例

