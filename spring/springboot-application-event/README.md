# spring event 事件机制

## what
事件机制就是当发生某个事件时, 执行某个特定处理.

事件机制组成:
1. 事件定义, 比如事件的结构, 携带的数据
1. 事件生产者, 根据生产事件的条件和规则来生成事件
2. 事件消费者, 获取事件, 并根据事件的结构和数据进行处理

> 注意点:
> 1. 事件定义需要提前建模, 建模原则是可扩展, 可维护
> 2. 生产者和消费者都需要注意是否需要幂等性

## how: 快速开始

- 启动spring application 即可

1. 启动
2. 发送事件
3. 消费事件
4. 结束

> 配置额外的事件处理异步也可以的


