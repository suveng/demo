
# 
## 
**URL:** `/api/wechat/getUserInfo`

**Type:** `POST`

**Author:** mac

**Content-Type:** `application/x-www-form-urlencoded;charset=utf-8`

**Description:** 



**Query-parameters:**

Parameter|Type|Description|Required|Since
---|---|---|---|---
jsCode|string|No comments found.|false|-
ak|string|No comments found.|false|-
sk|string|No comments found.|false|-


**Request-example:**
```
curl -X POST -i /api/wechat/getUserInfo --data 'sk=m3g6zu&jsCode=56663&ak=y7yp02'
```
**Response-fields:**

Field | Type|Description|Since
---|---|---|---
openId|string|No comments found.|-

**Response-example:**
```
{
	"openId": "33"
}
```

