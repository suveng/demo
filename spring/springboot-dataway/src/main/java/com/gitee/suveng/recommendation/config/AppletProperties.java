/*
 * Cainiao.com Inc.
 * Copyright (c) 2013-2021 All Rights Reserved.
 */

package com.gitee.suveng.recommendation.config;

import lombok.Data;

/**
 * @author mac
 * @since 2021-05-02 4:04 下午
 */
@Data
public class AppletProperties {

    private String ak;

    private String sk;

    private String openIdUrl;
}