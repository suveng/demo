/*
 * Cainiao.com Inc.
 * Copyright (c) 2013-2021 All Rights Reserved.
 */

package com.gitee.suveng.recommendation.controller.model;

import lombok.Data;

/**
 * @author mac
 * @since 2021-05-02 5:31 下午
 */
@Data
public class WechatUserRequest {
    private String jsCode;
    private String ak;
    private String sk;
}