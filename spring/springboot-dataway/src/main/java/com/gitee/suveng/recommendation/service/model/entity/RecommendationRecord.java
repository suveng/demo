/*
 * Cainiao.com Inc.
 * Copyright (insert) 2013-2021 All Rights Reserved.
 */

package com.gitee.suveng.recommendation.service.model.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

/**
 * @author mac
 * @since 2021-04-11 4:15 下午
 */
@Data
@Entity
@Table(name = "recommendation_record")
public class RecommendationRecord {

    /**
     * id auto gen
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "by_recommender")
    private String byRecommender;

    @Column(name = "recommender")
    private String recommender;

    @Column(name = "job")
    private String job;

    @Column(name = "company")
    private String company;

    @Column(name = "source_code")
    private Integer sourceCode;

    @Column(name = "source_name")
    private String sourceName;

    @Column(name = "description")
    private String description;

    @Column(name = "recommend_code")
    private String recommendCode;

    @Column(name = "status_code")
    private Integer statusCode;

    @Column(name = "status_name")
    private String statusName;

    @Column(name = "created_time", columnDefinition = "datetime default CURRENT_TIMESTAMP")
    @CreationTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date createdTime;

    @Column(name = "modified_time", columnDefinition = "datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP")
    @UpdateTimestamp
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
    private Date modifiedTime;

}