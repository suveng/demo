/*
 * Cainiao.com Inc.
 * Copyright (c) 2013-2021 All Rights Reserved.
 */

package com.gitee.suveng.recommendation.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.NestedConfigurationProperty;
import org.springframework.context.annotation.Configuration;

/**
 * @author mac
 * @since 2021-05-02 4:03 下午
 */
@Configuration
@ConfigurationProperties(prefix = "wechat")
@Data
public class WechatEncryptionProperties {

    /**
     * 小程序配置
     */
    @NestedConfigurationProperty
    private AppletProperties applet;
}