/*
 * Cainiao.com Inc.
 * Copyright (c) 2013-2021 All Rights Reserved.
 */

package com.gitee.suveng.recommendation.config;

import java.util.concurrent.TimeUnit;

import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author mac
 * @since 2021-05-02 4:33 下午
 */
@Configuration
public class CacheConfig {

    /**
     * 配置缓存管理器
     *
     * @return token缓存管理器
     */
    @Bean("token")
    public CacheManager token() {
        CaffeineCacheManager cacheManager = new CaffeineCacheManager();
        cacheManager.setCaffeine(Caffeine.newBuilder()
            // 设置最后一次写入或访问后经过固定时间过期
            .expireAfterAccess(5000, TimeUnit.SECONDS)
            // 初始的缓存空间大小
            .initialCapacity(100)
        );
        return cacheManager;
    }
}