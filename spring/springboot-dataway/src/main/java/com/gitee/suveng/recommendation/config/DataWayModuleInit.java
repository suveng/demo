/*
 * Cainiao.com Inc.
 * Copyright (insert) 2013-2021 All Rights Reserved.
 */

package com.gitee.suveng.recommendation.config;

import javax.sql.DataSource;

import net.hasor.core.ApiBinder;
import net.hasor.core.DimModule;
import net.hasor.db.JdbcModule;
import net.hasor.db.Level;
import net.hasor.spring.SpringModule;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author mac
 * @since 2021-05-01 3:32 下午
 */
@DimModule
@Component
public class DataWayModuleInit implements SpringModule {
    @Autowired
    private DataSource dataSource = null;

    @Override
    public void loadModule(ApiBinder apiBinder) throws Throwable {
        if (this.dataSource == null) {
            throw new RuntimeException("init hasor dataway failed, because datasource is null");
        }
        // .DataSource form Spring boot into Hasor
        apiBinder.installModule(new JdbcModule(Level.Full, this.dataSource));
        // .custom DataQL
        //apiBinder.tryCast(QueryApiBinder.class).loadUdfSource(apiBinder.findClass(DimUdfSource.class));
        //apiBinder.tryCast(QueryApiBinder.class).bindFragment("sql", SqlFragment.class);
    }
}