/*
 * Cainiao.com Inc.
 * Copyright (c) 2013-2021 All Rights Reserved.
 */

package com.gitee.suveng.recommendation.service.cache.dao.impl;

import java.util.HashMap;

import javax.annotation.Resource;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import cn.hutool.http.HttpUtil;
import com.gitee.suveng.recommendation.config.AppletProperties;
import com.gitee.suveng.recommendation.config.WechatEncryptionProperties;
import com.gitee.suveng.recommendation.controller.model.WechatUserRequest;
import com.gitee.suveng.recommendation.service.cache.dao.WechatUserCacheMapper;
import com.gitee.suveng.recommendation.service.cache.model.WechatUser;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

/**
 * @author mac
 * @since 2021-05-02 4:55 下午
 */
@Service
@CacheConfig(cacheNames = "token", cacheManager = "token")
public class WechatUserCacheMapperImpl implements WechatUserCacheMapper {

    @Resource
    WechatEncryptionProperties encryptionProperties;

    @Override
    @Cacheable(key = "#request.jsCode", condition = "#result.openId ne null && #result.openId ne ''")
    public WechatUser getUserInfo(WechatUserRequest request) {
        AppletProperties applet = encryptionProperties.getApplet();
        String ak = applet.getAk();
        String sk = applet.getSk();
        String openIdUrl = applet.getOpenIdUrl();
        HashMap<String, Object> paramMap = new HashMap<>();
        paramMap.put("appid", ak);
        paramMap.put("secret", sk);
        paramMap.put("js_code", request.getJsCode());
        paramMap.put("grant_type", "authorization_code");
        String response = HttpUtil.get(openIdUrl, paramMap, 3000);
        Assert.notNull(response,"返回不能为空");
        JSONObject jsonObject = JSON.parseObject(response);
        String openid = jsonObject.getString("openid");
        WechatUser wechatUser = new WechatUser();
        wechatUser.setOpenId(openid);
        return wechatUser;
    }
}