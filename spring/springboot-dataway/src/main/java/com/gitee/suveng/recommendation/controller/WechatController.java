/*
 * Cainiao.com Inc.
 * Copyright (c) 2013-2021 All Rights Reserved.
 */

package com.gitee.suveng.recommendation.controller;

import com.gitee.suveng.recommendation.controller.model.WechatUserRequest;
import com.gitee.suveng.recommendation.service.cache.dao.WechatUserCacheMapper;
import com.gitee.suveng.recommendation.service.cache.model.WechatUser;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author mac
 * @since 2021-05-02 5:29 下午
 */
@RestController
@RequestMapping("/api/wechat")
public class WechatController {
    private final WechatUserCacheMapper wechatUserCacheMapper;

    public WechatController(
        WechatUserCacheMapper wechatUserCacheMapper) {this.wechatUserCacheMapper = wechatUserCacheMapper;}

    @PostMapping("/getUserInfo")
    public WechatUser getUserInfo(WechatUserRequest request) {
        return wechatUserCacheMapper.getUserInfo(request);
    }
}