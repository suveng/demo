/*
 * Cainiao.com Inc.
 * Copyright (c) 2013-2021 All Rights Reserved.
 */

package com.gitee.suveng.recommendation.service.cache.model;

import lombok.Data;

/**
 * @author mac
 * @since 2021-05-02 4:52 下午
 */
@Data
public class WechatUser {
    private String openId;


}