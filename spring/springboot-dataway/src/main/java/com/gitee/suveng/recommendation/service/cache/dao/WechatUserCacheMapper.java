/*
 * Cainiao.com Inc.
 * Copyright (c) 2013-2021 All Rights Reserved.
 */

package com.gitee.suveng.recommendation.service.cache.dao;

import com.gitee.suveng.recommendation.controller.model.WechatUserRequest;
import com.gitee.suveng.recommendation.service.cache.model.WechatUser;

/**
 * @author mac
 * @since 2021-05-02 4:54 下午
 */
public interface WechatUserCacheMapper {
    WechatUser getUserInfo(WechatUserRequest request);
}