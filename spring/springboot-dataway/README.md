# 内推服务端

## 架构
1. dataway 开发curd的接口
2. springboot jpa
3. 基于H2数据库
4. smart-doc 生成文件存放在doc目录下
5. internal_recommendation.mv.db 是 h2数据的源文件, 在配置中可以看到


## 快速开始

- 查看配置文件
- 创建必备表, jpa开启create-drop模式, 指定初始化文件 dataway_4.2.0-h2_ddl.sql
- 启动spring application
- 没有报错即可启动成功





