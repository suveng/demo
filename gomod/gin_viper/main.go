package main

import (
	"github.com/gin-gonic/gin"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"net/http"
)

func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method
		c.Header("Access-Control-Allow-Origin", "*")
		c.Header("Access-Control-Allow-Headers", "Content-Type,AccessToken,X-CSRF-Token, Authorization, Token")
		c.Header("Access-Control-Allow-Methods", "POST, GET, OPTIONS")
		c.Header("Access-Control-Expose-Headers", "Content-Length, Access-Control-Allow-Origin, Access-Control-Allow-Headers, Content-Type")
		c.Header("Access-Control-Allow-Credentials", "true")
		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		// 处理请求
		c.Next()
	}
}

func main() {
	r := gin.Default()
	r.Use(Cors())
	gin.SetMode(viper.GetString("gin.mode"))
	port := viper.GetString("gin.port")

	logrus.Info("gin running!")
	// health endpoint
	buildHealthEndpoint(r)
	if err := r.Run(port); err != nil {
		logrus.Fatal("gin run fail!")
	}
}

func buildHealthEndpoint(r *gin.Engine) {
	r.GET("/health", func(context *gin.Context) {
		context.JSON(200, gin.H{
			"ok": true,
		})
	})
}
