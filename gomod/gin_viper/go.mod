module gin_viper

go 1.16

require (
	github.com/fsnotify/fsnotify v1.4.7
	github.com/gin-gonic/gin v1.6.3
	github.com/sirupsen/logrus v1.7.0
	github.com/spf13/viper v1.7.1
)
