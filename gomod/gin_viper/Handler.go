package main

import (
	"gin_viper/config"
	"github.com/sirupsen/logrus"
)

func init() {
	logrus.Info("初始化viper")
	// 初始化viper
	if err := config.Init(""); err != nil {
		return
	}
}
