package config

import (
	"github.com/fsnotify/fsnotify"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

type config struct {
	FileName string
}

func Init(filename string) error {
	c := config{FileName: filename}
	// 初始化viper配置
	if err := c.InitViper(); err != nil {
		return err
	}

	//监听配置文件变化,热加载配置
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		logrus.Info("Config file changed: %s", e.Name)
	})
	return nil
}

// 初始化viper配置
func (c *config) InitViper() error {
	// 获取配置文件
	if c.FileName != "" {
		viper.SetConfigName(c.FileName)
	} else {
		// 没有,则使用默认值
		viper.AddConfigPath("conf")
		viper.SetConfigName("default_config")
	}

	viper.SetConfigType("yaml")

	// 读取配置
	if err := viper.ReadInConfig(); err != nil {
		return err
	}

	return nil
}
