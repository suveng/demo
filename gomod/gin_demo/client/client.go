/*
 * Cainiao.com Inc.
 * Copyright (c) 2013-2021 All Rights Reserved.
 */

package main

import (
	"context"
	v1 "gin_demo/api/proto/v1"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"time"
)

const (
	address = "localhost:50051"
)

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		logrus.Info("链接失败: %v", err)
	}
	defer conn.Close()
	c := v1.NewTodoServiceClient(conn)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.Create(ctx, &v1.CreateRequest{Todo: nil})
	if err != nil {
		logrus.Fatalf("调用失败: %v", err)
	}

	logrus.Printf("Greeting: %s", r.Id)
}
