/*
 * Cainiao.com Inc.
 * Copyright (c) 2013-2021 All Rights Reserved.
 */

package api

type UserInfo interface {
	// 新增用户信息
	addUserInfo(user User)
	//获取用户信息
	getUserInfo(userId int32)
	//更新用户信息
	updateUserInfo(userId int32, user User)
	//删除用户信息
	deleteUserInfo(userId int32, logic bool)
}

type User struct {
}
