/*
 * Cainiao.com Inc.
 * Copyright (c) 2013-2021 All Rights Reserved.
 */

package main

import (
	"context"
	v1 "gin_demo/api/proto/v1"
	"github.com/sirupsen/logrus"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"net"
)

type server struct {
}

func (s *server) Create(ctx context.Context, in *v1.CreateRequest) (*v1.CreateResponse, error) {
	logrus.Infof("参数 %v", in.Todo)
	return &v1.CreateResponse{}, nil
}

func main() {
	listen, err := net.Listen("tcp", ":50051")
	if err != nil {
		logrus.Error("监听失败")
	}
	s := grpc.NewServer()
	v1.RegisterTodoServiceServer(
		s, &server{},
	)
	reflection.Register(s)
	oerror := s.Serve(listen)
	if oerror != nil {
		logrus.Error("启动失败")
	}
}
