import { UserOutlined } from '@ant-design/icons';

const IndexSiderRouterConfig = [
    {
      title: '控制台',
      key: '/index/console'
    },
    {
      title: '用户管理',
      icon: UserOutlined,
      key: '/index/user',
      child: [
        {key: '/index/user/list', title: '用户列表',},
        {
          key: '/index/user/add',
          title: '添加用户',
        }
      ]
    },
    {
      title: '部门管理',
      icon: UserOutlined,
      key: '/index/department',
      child: [
        {key: '/index/department/list', title: '部门列表', },
        {key: '/index/department/add', title: '添加部门', },
      ]
    },
    {
      title: '职位管理',
      icon: UserOutlined,
      key: '/home/entry',
      child: [
        {key: '/home/entry/form/basic-form', title: '职位列表', },
        {key: '/home/entry/form/step-form', title: '添加职位', }
      ]
    },
    {
        title: '任务中心',
        icon: UserOutlined,
        key: '/index/task_center',
        child: [
            {key: '/index/task_center/list', title: '任务列表',},
            {
                key: '/index/task_center/add',
                title: '添加任务',
            }
        ]
    },
  ]

  export default IndexSiderRouterConfig;
