package my.suveng.sharding.config;

import java.util.Map;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author suwenguang
 * @date 2020/11/3 11:08 下午
 * @since 1.0
 */
@Configuration
@ConfigurationProperties(prefix = "config.route")
@Data
public class DataSourceConfigProperties {

	private Map<String, Integer> weight;

}
