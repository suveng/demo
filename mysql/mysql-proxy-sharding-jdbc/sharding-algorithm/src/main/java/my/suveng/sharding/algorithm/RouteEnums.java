/*
 * Cainiao.com Inc.
 * Copyright (c) 2013-2021 All Rights Reserved.
 */

package my.suveng.sharding.algorithm;

/**
 * @author suwenguang
 * @since 2021-01-06 20:55
 */
public enum RouteEnums {
	//
	v001("v001", DataSourceRouteV1.class),
	;

	RouteEnums(String version, Class<? extends AbstractDataSourceRoute> impl) {
		this.version = version;
		this.impl = impl;
	}

	private final String version;

	private final Class<? extends AbstractDataSourceRoute> impl;

	public String getVersion() {
		return version;
	}

	public Class<? extends AbstractDataSourceRoute> getImpl() {
		return impl;
	}
}
