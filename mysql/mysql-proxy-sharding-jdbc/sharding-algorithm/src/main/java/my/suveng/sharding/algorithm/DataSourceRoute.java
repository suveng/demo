package my.suveng.sharding.algorithm;

import my.suveng.sharding.util.SpringUtil;

/**
 * @author suwenguang
 * @date 2020/11/3 11:21 下午
 * @since 1.0
 */
public interface DataSourceRoute {

	static DataSourceRoute get(String version) {
		// todo://配置到数据库
		RouteEnums route = RouteEnums.valueOf(version);
		Class<? extends AbstractDataSourceRoute> impl = route.getImpl();
		return SpringUtil.getBean(impl);
	}

	String route(Object id);
}
