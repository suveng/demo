drop table if exists sharding_test;
CREATE TABLE `sharding_test`
(
    `id`        varchar(128)  NOT NULL COMMENT '自增id',
    number      int(18)             NOT NULL default 0 COMMENT 'number',
    number_key  bigint(20)          not null default 0 comment 'number key',
    message     varchar(128)        not null default '' comment 'message',
    properties  json comment '扩展字段',
    create_time datetime                     default CURRENT_TIMESTAMP not null comment '创建时间',
    modify_time datetime                     default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP comment '修改时间,更新则修改',
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8 comment '测试表'
;
