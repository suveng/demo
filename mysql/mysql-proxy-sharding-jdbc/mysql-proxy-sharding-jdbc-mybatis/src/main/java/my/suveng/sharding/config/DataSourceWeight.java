package my.suveng.sharding.config;

import lombok.Data;

/**
 * @author suwenguang
 * @date 2020/11/3 11:18 下午
 * @since 1.0
 */
@Data
public class DataSourceWeight {

	/**
	 * 权重
	 */
	private Integer weight;
}
