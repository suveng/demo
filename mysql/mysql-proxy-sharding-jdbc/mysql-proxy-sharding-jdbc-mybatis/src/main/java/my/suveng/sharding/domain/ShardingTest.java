package my.suveng.sharding.domain;

import java.util.Date;

/**
 * 测试表
 *
 * @author 苏文广
 * @date 2020/11/03 16:55:28
 */
public class ShardingTest {
    /**
     * 自增id
     */
    private String id;

    /**
     * number
     */
    private Integer number;

    /**
     * number key
     */
    private Long numberKey;

    /**
     * message
     */
    private String message;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 修改时间,更新则修改
     */
    private Date modifyTime;

    /**
     * 扩展字段, 已读用户,未读用户, 异常用户
     */
    private String properties;

    /** getter */
    public String getId() {
        return id;
    }

    /** setter */
    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    /** getter */
    public Integer getNumber() {
        return number;
    }

    /** setter */
    public void setNumber(Integer number) {
        this.number = number;
    }

    /** getter */
    public Long getNumberKey() {
        return numberKey;
    }

    /** setter */
    public void setNumberKey(Long numberKey) {
        this.numberKey = numberKey;
    }

    /** getter */
    public String getMessage() {
        return message;
    }

    /** setter */
    public void setMessage(String message) {
        this.message = message == null ? null : message.trim();
    }

    /** getter */
    public Date getCreateTime() {
        return createTime;
    }

    /** setter */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /** getter */
    public Date getModifyTime() {
        return modifyTime;
    }

    /** setter */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }

    /** getter */
    public String getProperties() {
        return properties;
    }

    /** setter */
    public void setProperties(String properties) {
        this.properties = properties == null ? null : properties.trim();
    }
}