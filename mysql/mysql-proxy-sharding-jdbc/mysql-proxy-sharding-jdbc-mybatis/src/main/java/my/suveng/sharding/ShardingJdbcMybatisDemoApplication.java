package my.suveng.sharding;

import my.suveng.sharding.config.DataSourceConfigProperties;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

/**
 * 分库分表
 */
@SpringBootApplication
@MapperScan("my.suveng.sharding.dao")
@EnableConfigurationProperties(DataSourceConfigProperties.class)
public class ShardingJdbcMybatisDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShardingJdbcMybatisDemoApplication.class, args);
	}

}
