package my.suveng.sharding.config;

import java.util.Map;

import lombok.Data;

/**
 * @author suwenguang
 * @since 2020/11/3 11:24 下午
 */
@Data
public class DataSourceRouteConfig {
	/**
	 * 数据源权重
	 */
	private Map<String, DataSourceWeight> datasourceWeight;
}
