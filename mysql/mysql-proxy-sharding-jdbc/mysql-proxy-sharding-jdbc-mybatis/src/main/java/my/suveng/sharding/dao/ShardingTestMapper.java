package my.suveng.sharding.dao;

import java.util.List;
import my.suveng.sharding.domain.ShardingTest;
import my.suveng.sharding.domain.ShardingTestExample;
import org.apache.ibatis.annotations.Param;

public interface ShardingTestMapper {
    /** 
     * generate
     */ 
    long countByExample(ShardingTestExample example);

    /** 
     * generate
     */ 
    int deleteByExample(ShardingTestExample example);

    /** 
     * generate
     */ 
    int deleteByPrimaryKey(String id);

    /** 
     * generate
     */ 
    int insert(ShardingTest record);

    /** 
     * generate
     */ 
    int insertSelective(ShardingTest record);

    /** 
     * generate
     */ 
    List<ShardingTest> selectByExampleWithBLOBs(ShardingTestExample example);

    /** 
     * generate
     */ 
    List<ShardingTest> selectByExample(ShardingTestExample example);

    /** 
     * generate
     */ 
    ShardingTest selectByPrimaryKey(String id);

    /** 
     * generate
     */ 
    int updateByExampleSelective(@Param("record") ShardingTest record, @Param("example") ShardingTestExample example);

    /** 
     * generate
     */ 
    int updateByExampleWithBLOBs(@Param("record") ShardingTest record, @Param("example") ShardingTestExample example);

    /** 
     * generate
     */ 
    int updateByExample(@Param("record") ShardingTest record, @Param("example") ShardingTestExample example);

    /** 
     * generate
     */ 
    int updateByPrimaryKeySelective(ShardingTest record);

    /** 
     * generate
     */ 
    int updateByPrimaryKeyWithBLOBs(ShardingTest record);

    /** 
     * generate
     */ 
    int updateByPrimaryKey(ShardingTest record);
}