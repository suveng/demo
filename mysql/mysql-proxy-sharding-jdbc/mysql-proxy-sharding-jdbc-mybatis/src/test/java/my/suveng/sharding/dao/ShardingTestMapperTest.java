package my.suveng.sharding.dao;

import java.util.List;

import cn.hutool.core.util.RandomUtil;
import my.suveng.sharding.ShardingJdbcMybatisDemoApplicationTests;
import my.suveng.sharding.domain.ShardingTest;
import my.suveng.sharding.domain.ShardingTestExample;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author suwenguang
 * @date 2020/11/3 3:54 下午
 * @since 1.0
 */
class ShardingTestMapperTest extends ShardingJdbcMybatisDemoApplicationTests {

	@Autowired
	ShardingTestMapper shardingTestMapper;

	@Test
	public void setUp() {
		System.out.println("begin+++++");
		for (int i = 0; i < 1000; i++) {
			ShardingTest record = new ShardingTest();
			record.setId("v001" + RandomUtil.randomNumbers(5) + i);
			record.setNumber(RandomUtil.randomInt(1, 10000));
			record.setNumberKey(RandomUtil.randomLong());
			record.setMessage(RandomUtil.randomNumbers(4));
			shardingTestMapper.insertSelective(record);
		}

	}

	@Test
	public void tearDown() {
		for (int i = 0; i < 1000; i++) {
			shardingTestMapper.deleteByPrimaryKey(String.valueOf(i));
		}
		System.out.println("end+++++");

	}

	@Test
	void countByExample() {
	}

	@Test
	void deleteByExample() {
	}

	@Test
	void deleteByPrimaryKey() {
	}

	@Test
	void insert() {
	}

	@Test
	void insertSelective() {
	}

	@Test
	void selectByExampleWithBLOBs() {
	}

	@Test
	void selectByExample() {
		ShardingTestExample example = new ShardingTestExample();
		example.createCriteria().andCreateTimeIsNotNull();
		List<ShardingTest> shardingTests = shardingTestMapper.selectByExample(example);
	}

	@Test
	void selectByPrimaryKey() {
	}

	@Test
	void updateByExampleSelective() {
	}

	@Test
	void updateByExampleWithBLOBs() {
	}

	@Test
	void updateByExample() {
	}

	@Test
	void updateByPrimaryKeySelective() {
	}

	@Test
	void updateByPrimaryKeyWithBLOBs() {
	}

	@Test
	void updateByPrimaryKey() {
	}
}
